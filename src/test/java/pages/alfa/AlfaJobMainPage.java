package pages.alfa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class AlfaJobMainPage extends Page {
    private static final String URL = "http://job.alfabank.ru";

    public AlfaJobMainPage(WebDriver driver) {
        super(driver);
        this.url = URL;
    }

    public static class Locator {
        public static final By ABOUT_WORK_LINK = By.xpath("//span[@class='nav_item-link-helper' and contains(text(), 'О работе в банке')]");
    }

}
