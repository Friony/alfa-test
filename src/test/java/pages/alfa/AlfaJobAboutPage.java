package pages.alfa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class AlfaJobAboutPage extends Page {
    public static final String URL = "http://job.alfabank.ru/about";

    public AlfaJobAboutPage(WebDriver driver) {
        super(driver);
        this.url = URL;
    }

    public static class Locator {
        public static final By AFE_TEXT_TITLE = By.xpath("//div[@class='message']");
        public static final By AFE_TEXT_BODY =  By.xpath("//div[@class='info']/child::p");
    }


}
