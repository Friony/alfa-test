package pages.alfa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class AlfaMainPage extends Page {
    public static final String URL = "https://alfabank.ru/";

    public AlfaMainPage(WebDriver driver) {
        super(driver);
        this.url = URL;
    }

    public static class Locator {
        public static final By WORK_WITH_US_LINK = By.xpath("//a[contains(text(), 'Работайте у нас')]");
        public static final By FOOTER_DIV = By.xpath("//div[@class='footer__navigation']");
    }

}
