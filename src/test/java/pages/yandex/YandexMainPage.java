package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class YandexMainPage extends Page {
    private static final String URL = "http://yandex.ru";

    public static class Locator {
        public static final By MARKET_LINK = By.xpath("//*[@data-id='market']");
        public static final By REQUEST_INPUT = By.xpath("//*[@id='text']");
        public static final By SUBMIT_REQUEST_BUTTON = By.xpath("//button[@class='button suggest2-form__button button_theme_websearch button_size_ws-head i-bem button_js_inited']");
    }

    public YandexMainPage(WebDriver driver) {
        super(driver);
        this.url = URL;
    }

    public void makeSearch(String request) {
        System.out.println("Making search.");
        driver.findElement(Locator.REQUEST_INPUT).sendKeys(request);
        driver.findElement(Locator.SUBMIT_REQUEST_BUTTON).click();
    }

}
