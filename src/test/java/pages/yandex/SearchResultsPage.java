package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class SearchResultsPage extends Page {

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public static class Locator {
        public static final By PAGE_FIRST_NONADV_LINK = By.xpath("//div[@class='a11y-hidden']/ancestor::h2//a");
    }


}
