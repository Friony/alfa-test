package pages.yandex;

import org.openqa.selenium.WebDriver;
import pages.Page;

public class MarketMainPage extends Page {
    private static final String URL = "https://market.yandex.ru";

    public static class Locator {
        //not needed for now
    }

    public MarketMainPage(WebDriver driver) {
        super(driver);
        this.url = URL;
    }

}
