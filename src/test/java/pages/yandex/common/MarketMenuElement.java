package pages.yandex.common;

import exercises.WebDriverTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.TimeoutManager;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class MarketMenuElement {
    private WebDriver driver;

    public MarketMenuElement(WebDriver driver) {
        this.driver = driver;
    }

    public static class Locator {
        //--Depts--//
        public static final By ELECTRONICS_DEPT_LINK = By.xpath("//a[@class='link topmenu__link' and contains(text(), 'Электроника')]");
        //--Dropdown options--//
        //Electronics
        public static final By ELECTRONICS_MPHONES_DD = By.xpath("//a[@class='link topmenu__subitem topmenu__subitem_nid_54726']");
        public static final By ELECTRONICS_HEADPHONES_DD = By.xpath("//a[@class='link topmenu__subitem topmenu__subitem_nid_56179']");
    }

    public void clickDropDownElement(By cathegory, By dropdown) {
        Actions action = new Actions(driver);
        WebElement cat = driver.findElement(cathegory);
        action.moveToElement(cat).build().perform();

        TimeoutManager.changeImplWaitTimeout(driver, 0);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(dropdown)));
        TimeoutManager.setDefaultImpWaitTimeout(driver);

        action.moveToElement(driver.findElement(dropdown)).click().build().perform();
    }

    public void clickElement(By locator) {
        driver.findElement(locator).click();
    }


}
