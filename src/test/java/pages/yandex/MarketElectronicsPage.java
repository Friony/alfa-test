package pages.yandex;

import org.openqa.selenium.WebDriver;
import pages.Page;

public class MarketElectronicsPage extends Page {
    private static final String URL = "https://market.yandex.ru/catalog/54440";

    public MarketElectronicsPage(WebDriver driver) {
        super(driver);
        this.url = URL;
    }

    public static class Locator {
        //not needed for now
    }

}
