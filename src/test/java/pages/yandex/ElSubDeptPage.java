package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Page;
import utils.TimeoutManager;

public class ElSubDeptPage extends Page {

    public ElSubDeptPage(WebDriver driver) {
        super(driver);
    }

    public ElSubDeptPage(WebDriver driver, String url) {
        super(driver);
        this.url = url;
    }

    public static class Locator {
        //--Мобильные телефоны--//
        public static final By MANUFACTURER_SAMSUNG_CHECKBOX = By.xpath("//label[@class='checkbox__label' and contains(text(), 'Samsung')]");
        public static final By TYPE_SMARTPHONE_CHECKBOX = By.xpath("//label[@class='checkbox__label' and contains(text(), 'смартфон')]");
        public static final By TYPE_FILTER = By.xpath("//span[@class='title__content' and contains(text(), 'Тип')]");
        //--Наушники и bluetooth-гарнитуры--//
        public static final By MANUFACTURER_BEATS_CHECKBOX = By.xpath("//label[@class='checkbox__label' and contains(text(), 'Beats')]");
        //--Общее--//
        public static final By PRICE_FROM_INPUTBOX = By.xpath("//input[@class='input__control' and @id='glf-pricefrom-var']");
        public static final By PRICE_TO_INPUTBOX = By.xpath("//input[@class='input__control' and @id='glf-priceto-var']");
        public static final By FIRST_PRODUCT_TITLE = By.xpath("//a[3]/h4");
        public static final By APPLY_FILTERS_BUTTON = By.xpath("//button[@class='button button_action_n-filter-apply button_size_s button_pseudo_yes button_theme_pseudo i-bem button_js_inited']");
        public static final By PRELOADER_LAYER = By.xpath("//div[@class='n-filter-applied-results__content preloadable i-bem preloadable_js_inited']");

    }

    public void setFilter(By filterLabel) {
        driver.findElement(filterLabel).click();
    }

    public void setFilter(By inputBox, String value) {
        driver.findElement(inputBox).sendKeys(value);
    }

    public void applyFilters() {
        driver.findElement(Locator.APPLY_FILTERS_BUTTON).click();
        TimeoutManager.changeImplWaitTimeout(driver, 0);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.attributeToBe(Locator.PRELOADER_LAYER, "style", "height: auto;"));
        TimeoutManager.setDefaultImpWaitTimeout(driver);
    }

}
