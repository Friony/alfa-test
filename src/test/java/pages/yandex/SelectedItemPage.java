package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class SelectedItemPage extends Page {

    public SelectedItemPage(WebDriver driver) {
        super(driver);
    }

    public SelectedItemPage(WebDriver driver, String url) {
        super(driver);
        this.url = url;
    }

    public static class Locator {
        public static final By PRODUCT_NAME = By.xpath("//h1[@itemprop='name']");
    }

}
