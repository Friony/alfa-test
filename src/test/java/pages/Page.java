package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class Page<P extends Page> {
    public String url;
    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public P open() {
        driver.navigate().to(url);
        return (P) this;
    }

    public void navigateTo(By link) {
        System.out.println("Clicking element located: " + link);
        driver.findElement(link).click();
    }

    public String getAttributeValue(By elementLocator, String attribute) {
        String value;

        System.out.println("Parsing " + attribute + " from element located: " + elementLocator);

        if ((value = driver.findElement(elementLocator).getAttribute(attribute)) != null) {
            return value;
        } else {
            System.out.println("Error in getAttributeValue(): attribute not found.");
            return null;
        }
    }

    public String getAttributeValues(By elementLocator, String attribute) {
        StringBuilder attributes = new StringBuilder();
        String value;

        System.out.println("Parsing " + attribute + " from elements located: " + elementLocator);

        List<WebElement> elements = new ArrayList();
        elements = driver.findElements(elementLocator);

        for (WebElement elem : elements) {
            if ((value = elem.getAttribute(attribute)) != null) {
                attributes.append(value);
            } else {
                System.out.println("Error in getAttributeValue(): attribute not found.");
                return null;
            }
        }
        return attributes.toString();
    }

}
