package utils;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileWriter {
    public static String testResultsPath = System.getProperty("user.dir") + "/test-results/";

    public FileWriter() { }

    public File createResultFile(WebDriver driver, String additionalParam) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
        String currentDateTime = dateFormat.format(new Date());
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();

        File file = new File(testResultsPath + currentDateTime + "_" + browserName + "_" + additionalParam + ".txt");

        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("File was not created.");
            e.printStackTrace();
        }

        System.out.println("File " + file.getAbsolutePath() + " created.");
        return file;
    }

    public void writeInFile(File file, String content) {
        try {
            BufferedWriter wr = new BufferedWriter(new java.io.FileWriter(file));
            wr.write(content);
            wr.close();
        } catch (IOException e) {
            System.out.println("Content not written to " + file.getName());
            e.printStackTrace();
        }
        System.out.println("Successful write to file " + file.getName());
    }

}
