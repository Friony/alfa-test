package utils;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FileParser {

    public FileParser() { }

    public String[] parse(File file, String separator) {
        String curLine = null;
        StringBuilder result = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((curLine = reader.readLine()) != null) {
                result.append(curLine);
            }
            reader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result.toString().split(separator);
    }

    public List<String> sortResults(String[] results, boolean reverseOrder) {
        if (reverseOrder) {
            Arrays.sort(results, new numbersAsStringsComparator().reversed());
        } else {
            Arrays.sort(results, new numbersAsStringsComparator());
        }
        return Arrays.asList(results);
    }

    class numbersAsStringsComparator implements Comparator<String> {
        public int compare(String s1, String s2)
        {
            return Integer.valueOf(s1).compareTo(Integer.valueOf(s2));
        }
    }
}
