package utils;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class TimeoutManager {
    private static int IMPLICITY_TIMEOUT = 30;
    private static int PAGELOAD_TIMEOUT = 30;

    public static void changeImplWaitTimeout(WebDriver driver, int sec) {
        driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
    }

    public static void setDefaultImpWaitTimeout(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(IMPLICITY_TIMEOUT, TimeUnit.SECONDS);
    }

    public static void changePageLoadTimeout(WebDriver driver, int sec) {
        driver.manage().timeouts().pageLoadTimeout(sec, TimeUnit.SECONDS);
    }

    public static void setDefaultPageLoadTimeout(WebDriver driver) {
        driver.manage().timeouts().pageLoadTimeout(PAGELOAD_TIMEOUT, TimeUnit.SECONDS);
    }

}
