package exercises;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.alfa.AlfaJobAboutPage;
import pages.alfa.AlfaJobMainPage;
import pages.alfa.AlfaMainPage;
import pages.yandex.SearchResultsPage;
import pages.yandex.YandexMainPage;
import utils.FileWriter;
import utils.TimeoutManager;

import java.io.File;

public class GettingAFETextTest extends WebDriverTest {
    private String searchEngine = "Yandex";
    private String request = "Альфа-Банк";

    @BeforeMethod
    public void openYandex() {
        System.out.println("| Setting preconditions: opening Yandex main page. |");
        (new YandexMainPage(driver)).open();
    }

    @Test
    public void getAfeTextFromAlfa() {
        (new YandexMainPage(driver)).makeSearch(request); // находим сайт через поисковик
        (new SearchResultsPage(driver)).navigateTo(SearchResultsPage.Locator.PAGE_FIRST_NONADV_LINK);
        switchToNewWindow();

        System.out.println("Waiting " + request + " page to load."); // ожидание загрузки сайта
        TimeoutManager.changeImplWaitTimeout(driver, 0);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(AlfaMainPage.Locator.FOOTER_DIV)));
        TimeoutManager.setDefaultImpWaitTimeout(driver);

        (new AlfaMainPage(driver)).navigateTo(AlfaMainPage.Locator.WORK_WITH_US_LINK); // переход на сайт alfa.job.ru
        (new AlfaJobMainPage(driver)).navigateTo(AlfaJobMainPage.Locator.ABOUT_WORK_LINK); // затем в раздел "О работе в банке"

        AlfaJobAboutPage aboutPage = new AlfaJobAboutPage(driver);
        StringBuilder textForEmployees = new StringBuilder(); // считывание и сохранение текста
        textForEmployees.append(aboutPage.getAttributeValue(AlfaJobAboutPage.Locator.AFE_TEXT_TITLE, "innerHTML"));
        textForEmployees.append(aboutPage.getAttributeValues(AlfaJobAboutPage.Locator.AFE_TEXT_BODY, "innerHTML"));

        FileWriter writer = new FileWriter(); // запись в файл
        File testResults = writer.createResultFile(driver, searchEngine);
        writer.writeInFile(testResults, textForEmployees.toString());
    }
}
