package exercises;

import org.testng.annotations.Test;
import utils.FileParser;
import java.io.File;

public class SortingNumbersTest {
    private static final String TEST_DATA_PATH = "src/test/resources/test-data/";
    public static String FILE_NAME = "numbers.txt";

    @Test
    public void sortFileAndPrintResults() {
        System.out.println("--- Running " + this.getClass().toString() + " ---");

        File fileToParse = new File(TEST_DATA_PATH + FILE_NAME);
        FileParser parser = new FileParser();
        String[] fileContents = parser.parse(fileToParse, ","); // считывание файла в массив (разбиение по запятой)

        // вывод результатов в прямом и обратном порядке
        System.out.println("Results sorted in straight order: " + parser.sortResults(fileContents, false));
        System.out.println("Results sorted in reversed order: " + parser.sortResults(fileContents, true));

        System.out.println("------------------------");
        System.out.println("------------------------");
    }

}
