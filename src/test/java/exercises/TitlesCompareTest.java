package exercises;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.yandex.common.MarketMenuElement;
import pages.yandex.ElSubDeptPage;
import pages.yandex.SelectedItemPage;
import pages.yandex.YandexMainPage;

import java.util.HashMap;
import java.util.Map;

public class TitlesCompareTest extends WebDriverTest {
    private By subSectOne;
    private By subSectTwo;
    private Map<By, String> filtersSetOne;
    private Map<By, String> filtersSetTwo;

    {   // иниализация тестовых данных
        subSectOne = MarketMenuElement.Locator.ELECTRONICS_MPHONES_DD;
        filtersSetOne = new HashMap<By, String>();
        filtersSetOne.put(ElSubDeptPage.Locator.PRICE_FROM_INPUTBOX, "40000");
        filtersSetOne.put(ElSubDeptPage.Locator.MANUFACTURER_SAMSUNG_CHECKBOX, null);
        filtersSetOne.put(ElSubDeptPage.Locator.TYPE_FILTER, null);
        filtersSetOne.put(ElSubDeptPage.Locator.TYPE_SMARTPHONE_CHECKBOX, null);

        subSectTwo = MarketMenuElement.Locator.ELECTRONICS_HEADPHONES_DD;
        filtersSetTwo = new HashMap<By, String>();
        filtersSetTwo.put(ElSubDeptPage.Locator.PRICE_FROM_INPUTBOX, "17000");
        filtersSetTwo.put(ElSubDeptPage.Locator.PRICE_TO_INPUTBOX, "25000");
        filtersSetTwo.put(ElSubDeptPage.Locator.MANUFACTURER_BEATS_CHECKBOX, null);
    }

    @BeforeMethod
    public void openMarket() {
        System.out.println("| Setting preconditions: opening Yandex Market start page. |");
        (new YandexMainPage(driver)).open().navigateTo(YandexMainPage.Locator.MARKET_LINK);
    }

    @Test
    public void subDeptsTest() {
        compareTitles(getTitles(subSectOne, filtersSetOne));
        compareTitles(getTitles(subSectTwo, filtersSetTwo));
    }

    public String[] getTitles(By subDeptLink, Map<By, String> filters) {
        String[] titles = new String[2];

        // переход в подраздел
        (new MarketMenuElement(driver)).clickDropDownElement(MarketMenuElement.Locator.ELECTRONICS_DEPT_LINK, subDeptLink);
        System.out.println("Navigating to subsection by: " + subDeptLink + ".");

        ElSubDeptPage ElSubPage = new ElSubDeptPage(driver); // проставление фильтров
        System.out.println("Setting up filters.");
        for (Map.Entry<By, String> entry : filters.entrySet()) {
            if (entry.getValue() != null) {
                ElSubPage.setFilter(entry.getKey(), entry.getValue());
            } else {
                ElSubPage.setFilter(entry.getKey());
            }
        }
        ElSubPage.applyFilters();

        // парсинг заголовка в списке
        titles[0] = ElSubPage.getAttributeValue(ElSubDeptPage.Locator.FIRST_PRODUCT_TITLE, "innerHTML");
        System.out.println("Product title in list: " + '"' + titles[0] + '"' + ", saved.");
        ElSubPage.navigateTo(ElSubDeptPage.Locator.FIRST_PRODUCT_TITLE); // переход в карточку продукта
        // парсинг заголовка на странице продукта
        SelectedItemPage productPage = new SelectedItemPage(driver);
        titles[1] = productPage.getAttributeValue(SelectedItemPage.Locator.PRODUCT_NAME, "innerHTML");
        System.out.println("Product title in card: " + '"' + titles[1] + '"' + ", saved.");

        return titles;
    }

    public void compareTitles(String[] titles) {
                try {
                    Assert.assertEquals(titles[0], titles[1]); // сравнение результатов парсинга
                    System.out.println("| PASS | Products titles are equal: " + titles[0] + " >> " + titles[1]);
                } catch (AssertionError error) {
                    System.out.println("| FAIL | Titles of products are different. Check original message for details: ");
                    error.printStackTrace();
                } catch (IndexOutOfBoundsException ex) {
                    System.out.println("Array should contain 2 elements.");
                    ex.printStackTrace();
            }
        }

}
