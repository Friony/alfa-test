package exercises;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import utils.TimeoutManager;

public class WebDriverTest {
    protected WebDriver driver;

    @BeforeClass
    public void setUp() {
        if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver");
        }
        else if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver.exe");
        }
        else {
            System.out.println("Set up chromedriver for your system manually.");
        }
        driver = new ChromeDriver();
        TimeoutManager.changeImplWaitTimeout(driver,30);
        TimeoutManager.changePageLoadTimeout(driver, 30);
        driver.manage().window().maximize();
        System.out.println("--- Running " + this.getClass().toString() + " ---");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
        System.out.println("------------------------");
        System.out.println("------------------------");
    }

    public void switchToNewWindow() {
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
    }

}
